package cz.cvut.fel.ts1.storage;

import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class ItemStockTest {
    StandardItem item;
    ItemStock itemStock;

    @BeforeEach
    void setUp() {
        int id = 1;
        String name = "item";
        int price = 33;
        String category = "category";
        int loyaltyPoints = 100;

        item = new StandardItem(id, name, price, category, loyaltyPoints);

        itemStock = new ItemStock(item);
    }

    @Test
    void constructorTest() {
        Assertions.assertEquals(item, itemStock.getItem());
        Assertions.assertEquals(0, itemStock.getCount());
    }

    @ParameterizedTest
    @MethodSource("provideIncreaseTestData")
    void increaseTest(int increaseBy, int expected) {
        itemStock.IncreaseItemCount(increaseBy);
        Assertions.assertEquals(expected, itemStock.getCount());
    }
    @ParameterizedTest
    @MethodSource("provideDecreaseTestData")
    void decreasedTest(int decreaseBy, int expected) {
        itemStock.decreaseItemCount(decreaseBy);
        Assertions.assertEquals(expected, itemStock.getCount());
    }

    private static Stream<Arguments> provideIncreaseTestData() {
        return Stream.of(
              Arguments.of(10, 10),
              Arguments.of(0, 0),
              Arguments.of(-10, -10)
        );
    }
    private static Stream<Arguments> provideDecreaseTestData() {
        return Stream.of(
              Arguments.of(10, -10),
              Arguments.of(0, 0),
              Arguments.of(-10, 10)
        );
    }
}
