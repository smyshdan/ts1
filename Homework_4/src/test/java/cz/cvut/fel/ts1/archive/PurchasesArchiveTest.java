package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import java.util.HashMap;



public class PurchasesArchiveTest {
    @Test
    public void testPutOrderToPurchasesArchive() {
        // Vytvoření mock objektů
        Order mockOrder = Mockito.mock(Order.class);
        Item mockItem = Mockito.mock(Item.class);
        ArrayList<Item> itemList = new ArrayList<>();
        itemList.add(mockItem);

        // Nastavení chování mock objektů
        Mockito.when(mockOrder.getItems()).thenReturn(itemList);
        Mockito.when(mockItem.getID()).thenReturn(1);

        // Vytvoření instance PurchasesArchive
        PurchasesArchive archive = new PurchasesArchive();

        // Akce
        archive.putOrderToPurchasesArchive(mockOrder);

        // Ověření
        Assertions.assertEquals(1, archive.getHowManyTimesHasBeenItemSold(mockItem));
        Mockito.verify(mockOrder).getItems(); // ověření, že byla zavolána metoda getItems
    }

    @Test
    public void testPrintItemPurchaseStatistics() {
        // Přesměrování System.out pro zachycení výstupu
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Vytvoření mock objektu ItemPurchaseArchiveEntry
        ItemPurchaseArchiveEntry mockEntry = Mockito.mock(ItemPurchaseArchiveEntry.class);
        Mockito.when(mockEntry.toString()).thenReturn("data");
        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<>();
        itemArchive.put(1, mockEntry);

        Order mockOrder = Mockito.mock(Order.class);

        ArrayList<Order> orderArchive = new ArrayList<>();

        orderArchive.add(mockOrder);

        // Vytvoření instance PurchasesArchive s mock objektem
        PurchasesArchive archive = new PurchasesArchive(itemArchive, orderArchive);

        // Akce
        archive.printItemPurchaseStatistics();

        // Ověření
        String expectedOutput = "ITEM PURCHASE STATISTICS:\r\ndata\r\n"; // + očekávaný výstup
//        Assertions.assertEquals(outContent.toString().trim(), expectedOutput.trim());
        Assertions.assertEquals(expectedOutput, outContent.toString());
        // Resetování System.out
        System.setOut(System.out);
    }

    @Test
    public void testItemPurchaseArchiveEntryCreation() {
        Item mockItem = Mockito.mock(Item.class);
        Mockito.when(mockItem.getID()).thenReturn(1);

        ItemPurchaseArchiveEntry entry = new ItemPurchaseArchiveEntry(mockItem);

        Assertions.assertEquals(mockItem, entry.getRefItem());
    }
}
