package cz.cvut.fel.ts1.shop;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import cz.cvut.fel.ts1.shop.*;
import cz.cvut.fel.ts1.storage.*;
import cz.cvut.fel.ts1.archive.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

//@ExtendWith(MockitoExtension.class)
public class EShopControllerTest {

    private EShopController controller;

    @Mock
    private Storage storage = new Storage();

    @Mock
    private PurchasesArchive archive = new PurchasesArchive();


    @BeforeEach
    void setUp() {
        archive = mock();
        storage = mock();
        controller = new EShopController(storage, archive, new ArrayList<ShoppingCart>(Arrays.asList(new ShoppingCart(), new ShoppingCart())));
    }

    @Test
    void testShoppingCartOperations() {
        ShoppingCart cart = controller.newCart();
        Item item1 = new StandardItem(1, "Test Item 1", 100, "TEST_CATEGORY", 5);
        Item item2 = new StandardItem(2, "Test Item 2", 200, "TEST_CATEGORY", 3);
        cart.addItem(item1);
        cart.addItem(item2);

        assertEquals(2, cart.getItemsCount());
        cart.removeItem(1);
        assertEquals(1, cart.getItemsCount());
    }

    @Test
    void testPurchaseProcess() throws NoItemInStorage {
        ShoppingCart cart = controller.newCart();
        Item item1 = new StandardItem(1, "Test Item 1", 100, "TEST_CATEGORY", 5);
        Item item2 = new StandardItem(2, "Test Item 2", 200, "TEST_CATEGORY", 3);
        cart.addItem(item1);
        cart.addItem(item2);

        when(storage.getItemCount(any(Item.class))).thenReturn(5);

        controller.purchaseShoppingCart(cart, "John Doe", "123 Main St");
        verify(storage, times(1)).processOrder(any(Order.class));
    }

    @Test
    void testStorageOperations() {
        Item item = new StandardItem(1, "Test Item", 100, "TEST_CATEGORY", 5);
        when(storage.getItemCount(any(Item.class))).thenReturn(5);
        controller.getStorage().insertItems(item, 5);
        assertEquals(5, controller.getStorage().getItemCount(item));

        when(storage.getItemCount(any(Item.class))).thenReturn(2);
        assertDoesNotThrow(() -> controller.getStorage().removeItems(item, 3));
        assertEquals(2, controller.getStorage().getItemCount(item));
    }

    @Test
    void testPurchaseArchive() {
        ShoppingCart cart = controller.newCart();
        Item item1 = new StandardItem(1, "Test Item 1", 100, "TEST_CATEGORY", 5);
        Item item2 = new StandardItem(2, "Test Item 2", 200, "TEST_CATEGORY", 3);
        storage.insertItems(item1, 10);
        storage.insertItems(item2, 10);
        cart.addItem(item1);
        cart.addItem(item2);

        assertDoesNotThrow(() -> controller.purchaseShoppingCart(cart, "John Doe", "123 Main St"));
        verify(archive, times(1)).putOrderToPurchasesArchive(any(Order.class));
    }
}