package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class OrderTest {
    @Test
    void testOrderConstructorWithNullCartAndName() {
        ShoppingCart cart = null;
        String customerName = null;
        Assertions.assertThrows(NullPointerException.class, () -> new Order(cart, customerName, "123 Main St"));
    }

    @Test
    void testOrderConstructorWithState() {
        ShoppingCart cart = new ShoppingCart();
        Order order = new Order(cart, "John Doe", "123 Main St", 5);

        Assertions.assertEquals(order.getItems(), cart.getCartItems());
        Assertions.assertEquals(order.getCustomerName(), "John Doe");
        Assertions.assertEquals(order.getCustomerAddress(), "123 Main St");
        Assertions.assertEquals(order.getState(), 5);
    }

    @Test
    void testOrderConstructorWithoutState() {
        ShoppingCart cart = new ShoppingCart();
        Order order = new Order(cart, "John Doe", "123 Main St");

        Assertions.assertEquals(order.getItems(), cart.getCartItems());
        Assertions.assertEquals(order.getCustomerName(), "John Doe");
        Assertions.assertEquals(order.getCustomerAddress(), "123 Main St");
        Assertions.assertEquals(order.getState(), 0);
    }

}
