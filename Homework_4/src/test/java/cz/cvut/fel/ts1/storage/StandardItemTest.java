package cz.cvut.fel.ts1.storage;

import static org.junit.jupiter.api.Assertions.assertEquals;

import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.stream.Stream;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.provider.Arguments;

public class StandardItemTest {


    @Test
    void constructorTest() {
        int id = 1;
        String name = "item";
        int price = 33;
        String category = "category";
        int loyaltyPoints = 100;

        StandardItem item = new StandardItem(id, name, price, category, loyaltyPoints);

        Assertions.assertEquals(item.getID(), id);
        Assertions.assertEquals(item.getName(), name);
        Assertions.assertEquals(item.getPrice(), price);
        Assertions.assertEquals(item.getCategory(), category);
        Assertions.assertEquals(item.getLoyaltyPoints(), loyaltyPoints);
    }
    @Test
    void copyOfItemTest() {
        StandardItem item = new StandardItem(1, "item", 33, "category", 100);

        StandardItem copy = item.copy();

        Assertions.assertEquals(item, copy);
    }

    @ParameterizedTest
    @MethodSource("provideEqualsTestData")
    void testPersonEquals(StandardItem a, StandardItem b, boolean expected) {
        Assertions.assertEquals(expected, a.equals(b));
    }

    private static Stream<Arguments> provideEqualsTestData() {
        return Stream.of(
              Arguments.of(new StandardItem(1, "item", 33, "category", 100),
                                      new StandardItem(1, "item", 33, "category", 100), true),
              Arguments.of(new StandardItem(1, "item", 33, "category", 100),
                                      new StandardItem(2, "item", 33, "category", 100), false),
              Arguments.of(new StandardItem(1, "item", 33, "category", 100),
                                      new StandardItem(1, "iten", 33, "category", 100), false),
              Arguments.of(new StandardItem(1, "item", 33, "category", 100),
                                      new StandardItem(1, "item", 34, "category", 100), false),
              Arguments.of(new StandardItem(1, "item", 33, "category", 100),
                                      new StandardItem(1, "item", 33, "cotegory", 100), false),
              Arguments.of(new StandardItem(1, "item", 33, "category", 100),
                                      new StandardItem(1, "item", 33, "categary", 101), false)
        );
    }


}



