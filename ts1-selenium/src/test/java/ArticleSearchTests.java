import pages.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ArticleSearchTests {
    private App app;

    public ArticleSearchTests() {
        app = new App();
    }

    @BeforeEach
    public void start() {
        app.start();
    }

    @AfterEach
    public void destroy() {
        app.destroy();
    }

    static Stream<Arguments> testData1() {
        return Stream.of(
                Arguments.of(
                        "A detector for page-level handwritten music object recognition based on deep learning",
                        "https://doi.org/10.1007/s00521-023-08216-6",
                        "20 January 2023"
                )
        );
    }

    static Stream<Arguments> testData2() {
        return Stream.of(
                Arguments.of(
                        "Model-based testing leveraged for automated web tests",
                        "https://doi.org/10.1007/s11219-021-09575-w",
                        "27 November 2021")
        );
    }

    static Stream<Arguments> testData3() {
        return Stream.of(
                Arguments.of(
                        "SLEEPREPLACER: a novel tool-based approach for replacing thread sleeps in selenium WebDriver test code",
                        "https://doi.org/10.1007/s11219-022-09596-z",
                        "12 August 2022")
        );
    }

    static Stream<Arguments> testData4() {
        return Stream.of(
                Arguments.of(
                        "A brief review of state-of-the-art object detectors on benchmark document images datasets",
                        "https://doi.org/10.1007/s10032-023-00431-0",
                        "25 April 2023")
        );
    }


    @ParameterizedTest
    @MethodSource({"testData1","testData2", "testData3", "testData4"})
    public void searchAndVerifyArticle(String title, String expectedDOI, String expectedDate) {
        SearchResultPage searchResultsPage = getSearchResultsPage();

        List<ArticleInfo> articles = searchResultsPage.getArticles();
        boolean found = false;
        for (ArticleInfo article : articles) {
            System.out.println("Checking article: Title='" + article.title() + "', DOI='" + article.doi() + "', Date='" + article.publicationDate() + "'");
            if (article.title().trim().equalsIgnoreCase(title.trim()) &&
                    article.publicationDate().trim().equals(expectedDate.trim()) &&
                    article.doi().trim().equals(expectedDOI.trim())) {
                found = true;
                break;
            }
        }
        assertTrue(found, "The article with the specified title, date, and DOI was not found.");
    }

    private SearchResultPage getSearchResultsPage() {
        MainPage mainPage = new MainPage(app.getDriver(), app.getWait());
        LoginPage loginPage = mainPage.navigateToLoginPage();
        loginPage.loginAs("dixogas402@rencr.com");
        loginPage.loginWithPassword("hackerhacker");

        AdvancedSearchPage advancedSearchPage = mainPage.navigateToAdvancedSearchPage();
        return advancedSearchPage.performSearch("Page Object Model ", "Selenium Testing", "2024");
    }
}
