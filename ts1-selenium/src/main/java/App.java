import pages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class App {
    private WebDriver driver;
    private WebDriverWait wait;

    public void run() {
        start();

        try {
            MainPage mainPage = new MainPage(driver, wait);
            LoginPage loginPage = mainPage.navigateToLoginPage();
            loginPage.loginAs("den2003k12@gmail.com");
            loginPage.loginWithPassword("Selenium12@test");

            AdvancedSearchPage advancedSearchPage = mainPage.navigateToAdvancedSearchPage();

            SearchResultPage searchResultsPage = advancedSearchPage.performSearch("Page Object Model ","Selenium Testing", "2024"); // Adjust the search query and year as necessary

            List<ArticleInfo> articles = searchResultsPage.extractFirstFourArticles();
            for (ArticleInfo article : articles) {
                System.out.println("Title: " + article.title() + ", DOI: " + article.doi() + ", Publication Date: " + article.publicationDate());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        destroy();
    }

    public void start() {
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, Duration.of(3, ChronoUnit.SECONDS));
        driver.manage().deleteAllCookies();
        driver.get("https://link.springer.com/");
    }

    public void destroy() {
        driver.quit();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebDriverWait getWait() {
        return wait;
    }
}
