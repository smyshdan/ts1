package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdvancedSearchPage {

    protected final WebDriver driver;
    protected WebDriverWait wait;
    @FindBy(xpath = "//*[@id=\"all-words\"]")
    private WebElement allWordSearchBox;

    @FindBy(xpath = "//*[@id=\"least-words\"]")
    private WebElement someWordSearchBox;

    @FindBy(xpath = "//*[@id=\"date-facet-mode\"]")
    private WebElement searchTypeDropdown;

    @FindBy(xpath = "//*[@id=\"facet-start-year\"]")
    private WebElement enterYear;

    @FindBy(xpath = "//*[@id=\"submit-advanced-search\"]")
    private WebElement searchButton;

    public AdvancedSearchPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(driver, this);
        try {
            By cookieConsentSelector = By.xpath("/html/body/section/div/div[2]/button[1]");
            WebElement cookieConsentButton = wait.until(ExpectedConditions.elementToBeClickable(cookieConsentSelector));
            cookieConsentButton.click();
            System.out.println("Cookie dismissed.");
        } catch (NoSuchElementException | TimeoutException e) {
            System.out.println("Cookie not present.");
        }
    }

    public void enterSearchQuery1(String query) {
        wait.until(ExpectedConditions.visibilityOf(allWordSearchBox));
        allWordSearchBox.sendKeys(query);
    }

    public void enterSearchQuery2(String query) {
        wait.until(ExpectedConditions.visibilityOf(someWordSearchBox));
        someWordSearchBox.sendKeys(query);
    }

    public void selectSearchType(String type) {
        wait.until(ExpectedConditions.elementToBeClickable(searchTypeDropdown));
        if (searchTypeDropdown.isEnabled()) {
            Select selectEle = new Select(searchTypeDropdown);
            selectEle.selectByVisibleText("in");
            System.out.println("Select element is enabled.");
        } else {
            System.out.println("Select element is not enabled.");
        }
        enterYear.sendKeys(type);
    }

    public void clickSearchButton() {
        wait.until(ExpectedConditions.elementToBeClickable(searchButton));
        searchButton.click();
    }

    public SearchResultPage performSearch(String query1, String query2,String type) {
        enterSearchQuery1(query1);
        enterSearchQuery2(query2);
        selectSearchType(type);
        clickSearchButton();
        return new SearchResultPage(driver, wait);
    }
}
