package pages;

public record ArticleInfo(String title, String doi, String publicationDate) {
}