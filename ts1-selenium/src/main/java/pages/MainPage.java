package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class MainPage{

    protected final WebDriver driver;
    protected WebDriverWait wait;
    @FindBy(xpath = "//*[@id=\"identity-account-widget\"]")
    private WebElement loginLink;

    @FindBy(xpath = "//a[contains(text(),'Advanced Search')]")
    private WebElement advancedSearchLink;

    public MainPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(driver, this);
    }

    public LoginPage navigateToLoginPage() {
        try {
            WebElement consentDismiss = driver.findElement(By.cssSelector("body > dialog > div.cc-banner__content > div > div.cc-banner__footer > button.cc-button.cc-button--secondary.cc-button--contrast.cc-banner__button.cc-banner__button-accept"));
            consentDismiss.click();
        } catch (Exception e) {
            System.out.println("No banner.");
        }
        wait.until(elementToBeClickable(loginLink));
        loginLink.click();

        return new LoginPage(driver, wait);
    }

    public AdvancedSearchPage navigateToAdvancedSearchPage() {
        driver.get("https://link.springer.com/advanced-search");
        return new AdvancedSearchPage(driver, wait);
    }
}
