package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {

    protected final WebDriver driver;
    protected WebDriverWait wait;
    @FindBy(id = "username")
    private WebElement usernameField;

    @FindBy(xpath = "//*[@id=\"login-password\"]")
    private WebElement passwordField;

    @FindBy(xpath = "//*[@id=\"login-email\"]")
    private WebElement emailField;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement loginBtn;

    @FindBy(id = "password-submit")
    private WebElement continueBtn;

    public LoginPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(driver, this);
        try {
            By cookieConsentSelector = By.xpath("/html/body/section/div/div[2]/button[1]");
            WebElement cookieConsentButton = wait.until(ExpectedConditions.elementToBeClickable(cookieConsentSelector));
            cookieConsentButton.click();
            System.out.println("Cookie dismissed.");
        } catch (NoSuchElementException | TimeoutException e) {
            System.out.println("Cookie not present.");
        }
    }

    public void loginAs(String email) {
        enterEmail(email);
        clickLoginButton();
    }

    public void enterUsername(String username) {
        wait.until(ExpectedConditions.visibilityOf(usernameField));
        usernameField.sendKeys(username);
    }

    public void enterPassword(String password) {
        wait.until(ExpectedConditions.visibilityOf(passwordField));
        passwordField.sendKeys(password);
    }

    public void loginWithPassword(String password) {
        enterPassword(password);
        clickLoginButton();
    }

    public void clickContinueButton() {
        wait.until(ExpectedConditions.elementToBeClickable(continueBtn));
        continueBtn.click();
    }

    public void enterEmail(String email) {
        wait.until(ExpectedConditions.visibilityOf(emailField));
        emailField.sendKeys(email);
    }

    public void clickLoginButton() {
        wait.until(ExpectedConditions.elementToBeClickable(loginBtn));
        loginBtn.click();
    }

}
